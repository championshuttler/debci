module Debci
  module Validators
    module APTSource
      def invalid_extra_apt_sources(extra_apt_sources)
        invalid_extra_apt_sources = []
        Array(extra_apt_sources).each do |extra_apt_source|
          invalid_extra_apt_sources.push(extra_apt_source) unless !Debci.extra_apt_sources_list.find(extra_apt_source).nil? && Debci.extra_apt_sources_list.find(extra_apt_source).allowed?(@user)
        end
        invalid_extra_apt_sources.join(", ")
      end
    end
  end
end
